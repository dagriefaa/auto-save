<div align="center">
<img src="header.png" alt="Auto Save" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=2997727829<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/2997727829?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/2997727829?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/2997727829?color=%231b2838&logo=steam&style=for-the-badge)
</div>

This mod adds autosave functionality for machines.
- Saving is multithreaded - game will not freeze when saving
    - Saving the thumbnail is *not* multithreaded and may freeze the game a bit - disable them in config if that's a problem
- Saves every 60 seconds (configurable) if the current machine has been changed in some way - will not save on freshly loaded or deleted machines
- Autosaves are sorted into folders per-machine
- Automatically deletes old autosaves for the current machine if more than 25 autosaves (configurable) are saved
- Automatically cleans up autosaves older than 7 days (configurable)

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
You literally just run it.

[Object Explorer](https://gitlab.com/dagriefaa/object-explorer) is used for configuration. Config is currently set on `ModControllerObject.AutoSaver`.

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.
