[img=https://i.imgur.com/Qz5d98H.png][/img]
                             [b][url=https://besiege.fandom.com]Besiege Wiki[/url][/b]  |  [b][url=https://gitlab.com/dagriefaa/block-tooltips]GitLab Repository[/url][/b]

[img=https://i.imgur.com/9rOFiPs.png][/img]

This mod adds intelligent and configurable autosaving for machines.
- Autosaves happen every 60 seconds, but only if the current machine has been changed. Freshly loaded/deleted machines will not be autosaved until you change something.
- Autosaves are multithreaded - the game will not freeze while saving. (The thumbnails are not multithreaded, but you can turn them off if that's a problem.)
- Autosaves are sorted into folders based on the name of the machine. If more than 25 autosaves are present, old ones will be deleted.
- Autosaves are automatically cleaned up if they're over 7 days old.

[b]Configuration[/b] - via Object Explorer -> ModControllerObject.AutoSaver
- Autosave interval (seconds)
- Delete autosaves older than (days)
- Max files per machine
- Whether to save thumbnails or not