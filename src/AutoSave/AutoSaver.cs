﻿using Localisation;
using Modding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace AutoSave {
    /// <summary>
    /// saves:
    /// - every 60 seconds
    /// - when there has been at least one edit 
    /// - to the CURRENT machine
    /// </summary>
    internal class AutoSaver : MonoBehaviour {

        class FileMaker : LocalMachineCollection {
            public override CreateFileResult CreateFile(string fileName, out VirtualFile virtualObject) => base.CreateFile(fileName, out virtualObject);

            public VirtualFolder ChangeFolder(string name) {
                if (name == THUMBNAIL_FOLDER) {
                    throw new ArgumentException("Thumbnail folder is not accessible.");
                }
                var folder = CurrentFolder.GetObjects().FirstOrDefault(x => x.Name == name && x is VirtualFolder) as VirtualFolder;
                if (folder == null) {
                    CreateFolder(name);
                    folder = CurrentFolder.GetObjects().First(x => x.Name == name && x is VirtualFolder) as VirtualFolder;
                }
                ChangeFolder(folder);
                return folder;
            }
        }

        internal DynamicText MainText = null;
        internal DynamicText SubtitleText = null;

        int? _saveIntervalSeconds = null;
        public int SaveIntervalSeconds {
            get {
                if (_saveIntervalSeconds != null) { return (int)_saveIntervalSeconds; }
                SaveIntervalSeconds = Configuration.GetData().HasKey(nameof(SaveIntervalSeconds))
                    ? Configuration.GetData().ReadInt(nameof(SaveIntervalSeconds))
                    : 60;
                return SaveIntervalSeconds;
            }
            set {
                if (value == _saveIntervalSeconds) { return; }
                _saveIntervalSeconds = value;
                Configuration.GetData().Write(nameof(SaveIntervalSeconds), value);
            }
        }

        int? _deleteIntervalDays;
        public int DeleteIntervalDays {
            get {
                if (_deleteIntervalDays != null) { return (int)_deleteIntervalDays; }
                DeleteIntervalDays = Configuration.GetData().HasKey(nameof(DeleteIntervalDays))
                    ? Configuration.GetData().ReadInt(nameof(DeleteIntervalDays))
                    : 7;
                return DeleteIntervalDays;
            }
            set {
                if (value == _deleteIntervalDays) { return; }
                _deleteIntervalDays = value;
                Configuration.GetData().Write(nameof(DeleteIntervalDays), value);
            }
        }

        int? _maxFilesPerMachine;
        public int MaxFilesPerMachine {
            get {
                if (_maxFilesPerMachine != null) { return (int)_maxFilesPerMachine; }
                MaxFilesPerMachine = Configuration.GetData().HasKey(nameof(MaxFilesPerMachine))
                    ? Configuration.GetData().ReadInt(nameof(MaxFilesPerMachine))
                    : 25;
                return MaxFilesPerMachine;
            }
            set {
                if (value == _maxFilesPerMachine) { return; }
                _maxFilesPerMachine = value;
                Configuration.GetData().Write(nameof(MaxFilesPerMachine), value);
            }
        }

        bool? _saveThumbnails;
        public bool SaveThumbnails {
            get {
                if (_saveThumbnails != null) { return (bool)_saveThumbnails; }
                SaveThumbnails = Configuration.GetData().HasKey(nameof(SaveThumbnails))
                    ? Configuration.GetData().ReadBool(nameof(SaveThumbnails))
                    : false;
                return SaveThumbnails;
            }
            set {
                if (value == _saveThumbnails) { return; }
                _saveThumbnails = value;
                Configuration.GetData().Write(nameof(SaveThumbnails), value);
            }
        }

        private const string THUMBNAIL_FOLDER = "Thumbnails";
        private const int MAX_THUMBNAIL_TIME = 250;
        private static readonly DateTime REF_TIME = new DateTime(2014, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private System.Diagnostics.Stopwatch mainThreadTimer = new System.Diagnostics.Stopwatch();
        private System.Diagnostics.Stopwatch fileThreadTimer = new System.Diagnostics.Stopwatch();

        private Thread currentThread = null;
        private string directory;
        private string filename;
        private bool folderThumbExists;
        private Exception threadException = null;

        private bool machineJustDestroyed = true;
        internal bool MachineUpdatedSinceLastSave { get; private set; }
        internal DateTime NextRun { get; private set; }

        void Start() {
            PruneOldFiles();
            NextRun = DateTime.Now.AddSeconds(SaveIntervalSeconds);

            // do this song and dance so that it doesn't autosave when you load or delete a machine and then don't do anything
            ReferenceMaster.onMachineChanged += m => { MachineUpdatedSinceLastSave = false; };
            Events.OnMachineLoaded += OnMachineLoaded;
            Events.OnMachineDestroyed += () => { machineJustDestroyed = true; MachineUpdatedSinceLastSave = false; };

            ReferenceMaster.onMachineModified += m => {
                if (machineJustDestroyed) {
                    machineJustDestroyed = false;
                }
                else {
                    MachineUpdatedSinceLastSave = true;
                }
            };
        }

        private void OnMachineLoaded(Modding.Blocks.PlayerMachineInfo info) {
            machineJustDestroyed = true;
            MachineUpdatedSinceLastSave = false;

            // loaded autosave
            if (info.InternalObject.MachineData.HasKey(Mod.MOD_NAME)) {
                var name = info.InternalObject.MachineData.ReadString(Mod.MOD_NAME);
                if (!string.IsNullOrEmpty(name)) {
                    info.InternalObject.Name = name;
                    info.InternalObject.MachineData.Remove(Mod.MOD_NAME);
                }
            }
        }

        internal void OnSceneChanged() {
            Transform resetButton = Mod.FindObject("HUD/TopBar/Align (Top Left)/PlayButton");
            GameObject textOriginal = Mod.FindObject("HUD/TopBar/Align (Top Right)/SettingsContainer/Settings (Fold Out)/FPS/FPS").gameObject;
            var textObj = Instantiate(textOriginal, resetButton, false) as GameObject;
            textObj.transform.localPosition = new Vector3(-0.3f, -0.55f, 0);
            DestroyImmediate(textObj.GetComponent<LocalisationChild>());

            MainText = textObj.GetComponent<DynamicText>();
            MainText.alignment = TextAlignment.Left;
            MainText.anchor = DynamicTextAnchor.MiddleLeft;
            MainText.size = 0.16f;
            MainText.color = Color.clear;

            var subTextObj = Instantiate(MainText.gameObject, MainText.transform.parent, false) as GameObject;
            subTextObj.transform.localPosition = new Vector3(-0.3f, -0.65f, 0);
            SubtitleText = subTextObj.GetComponent<DynamicText>();
            SubtitleText.size = 0.14f;
            SubtitleText.anchor = DynamicTextAnchor.UpperLeft;
        }


        void Update() {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            // force save
            if (!ReferenceMaster.activeMachineSimulating && Mod.SaveKey.IsPressed) {
                AutoSave();
            }

            // start autosave thread
            if (NextRun < DateTime.Now) {
                NextRun = DateTime.Now.AddSeconds(SaveIntervalSeconds);
                if (MachineUpdatedSinceLastSave) {
                    MachineUpdatedSinceLastSave = false;
                    AutoSave();
                }
            }

            // on thread complete
            if (currentThread != null && !currentThread.IsAlive) {
                currentThread = null;

                if (threadException != null) {
                    Debug.LogError($"Exception when saving machine:");
                    Debug.LogException(threadException);
                    StartCoroutine(DoDisplayText($"AUTOSAVE FAILED", "CHECK CONSOLE FOR DETAILS", new Color(1, 0.3f, 0.3f, 1)));
                    return;
                }

                Debug.Log($"Machine saved in {fileThreadTimer.ElapsedMilliseconds}ms", this);

                if (SaveThumbnails && !ReferenceMaster.activeMachineSimulating) {
                    mainThreadTimer.Reset();
                    mainThreadTimer.Start();

                    float fov = Camera.main?.fieldOfView ?? -1;

                    // save thumbnail in main thread
                    var thumbnailCreator = GameObject.Find("HUD").transform.FindChild("FileBrowserView").GetComponent<ThumbnailCreator>();
                    thumbnailCreator.CaptureImage($"{directory}/{THUMBNAIL_FOLDER}/{filename}.png", false);

                    if (!folderThumbExists) {
                        thumbnailCreator.CaptureImage($"{directory}/thumb.png", false);
                    }

                    if (fov > -1) {
                        Camera.main.fieldOfView = fov;
                    }

                    mainThreadTimer.Stop();
                    Debug.Log($"Thumbnail saved in {mainThreadTimer.ElapsedMilliseconds}ms", this);
                }

                StartCoroutine(DoDisplayText($"AUTOSAVED", $"{Machine.Active().Name} -> {filename}.bsg"));
            }
        }

        IEnumerator DoDisplayText(string text, string subtitle, Color? c = null) {
            var baseColor = c ?? new Color(1, 1, 1, 0.37f);
            MainText.SetText(MainText.serializedText = text);
            SubtitleText.SetText(SubtitleText.serializedText = subtitle);
            MainText.color = SubtitleText.color = baseColor;
            yield return new WaitForSecondsRealtime(3f);
            while (MainText.color.a > 0) {
                MainText.color = SubtitleText.color = new Color(1, 1, 1, MainText.color.a - 0.002f);
                yield return new WaitForEndOfFrame();
            }
        }

        private void PruneOldFiles() {
            using (FileMaker fm = new FileMaker()) {
                fm.ChangeFolder(fm.GetRoot());
                var autosaveRoot = fm.CurrentFolder.GetObjects().FirstOrDefault(x => x.Name == Mod.MOD_NAME && x is VirtualFolder) as VirtualFolder;
                if (autosaveRoot == null) {
                    return;
                }
                fm.ChangeFolder(autosaveRoot);
                List<IVirtualObject> thingsToDelete = new List<IVirtualObject>();
                foreach (VirtualFolder folder in fm.CurrentFolder.GetObjects().Where(x => x is VirtualFolder)) {
                    fm.ChangeFolder(folder);

                    fm.CurrentFolder.GetObjects().Where(x => x is VirtualFile && x.Name != "thumb.png")
                        .Where(file => REF_TIME.AddSeconds((long)file.Date).AddDays(DeleteIntervalDays) < DateTime.Now).ToList() // spiderling why did you have to do timestamps like this
                        .ForEach(thingsToDelete.Add);

                    fm.ChangeFolder(folder);
                    if (fm.CurrentFolder.GetObjects().Where(x => x.Name != "thumb.png").Count() == 0) {
                        thingsToDelete.Add(folder);
                    }
                }
                fm.ChangeFolder(autosaveRoot);
                Debug.Log($"Found {thingsToDelete.Count} autosave objects to delete older than {DeleteIntervalDays} days", this);
                thingsToDelete.ForEach(x => x.Delete());
            }
        }

        public void AutoSave() {
            if (Mod.SceneNotPlayable() || !Machine.Active()) {
                return;
            }

            Debug.Log($"Starting autosave for machine {Machine.Active().Name}", this);

            currentThread = SaveMachine(Machine.Active().Name, Machine.Active().BuildingBlocks.ConvertAll(BlockInfo.FromBlockBehaviour));
            currentThread.IsBackground = true;
            currentThread.Start();
        }

        private Thread SaveMachine(string name, List<BlockInfo> blocks) {
            return new Thread(() => {
                try {

                    fileThreadTimer.Reset();
                    fileThreadTimer.Start();

                    filename = $"{DateTime.Now:MM-dd HH-mm-ss}";

                    // get directory + do other file operations
                    using (FileMaker fm = new FileMaker()) {
                        fm.ChangeFolder(fm.GetRoot());

                        fm.ChangeFolder(Mod.MOD_NAME); // make autosave root - in case it gets deleted for some reason
                        var folder = fm.ChangeFolder(name); // make machine dir
                        fm.CreateFolder(THUMBNAIL_FOLDER); // thanks spiderling for making the thumbnail folder inaccessible

                        // delete old files if exceeding set file count limit
                        if (folder.GetObjects().Count() > MaxFilesPerMachine) {
                            folder.GetObjects().OrderByDescending(x => x.Date).Skip(MaxFilesPerMachine - 1).ToList().ForEach(x => x.Delete());
                        }

                        folderThumbExists = folder.GetObjects().Any(x => x.Name.EndsWith(".png"));

                        directory = folder.ObjectPath.Path;
                    }

                    MachineInfo machineInfo = new MachineInfo {
                        Name = filename,
                        Position = Machine.Active().Position,
                        Rotation = Machine.Active().Rotation,
                        MachineData = Machine.Active().MachineData
                    };

                    machineInfo.MachineData.Write(Mod.MOD_NAME, name);

                    // sort nodes and edges to the top of the list to ensure they load properly
                    List<BlockInfo> others = new List<BlockInfo>();
                    foreach (BlockInfo b in blocks) {
                        switch (b.ID) {

                            case BlockType.BuildNode:
                                machineInfo.Blocks.Insert(0, b);
                                break;

                            case BlockType.BuildEdge:
                                machineInfo.Blocks.Add(b);
                                break;

                            default:
                                others.Add(b);
                                break;
                        }
                    }
                    machineInfo.Blocks.AddRange(others);

                    XmlSaver.Save(machineInfo, directory);
                }
                catch (Exception e) {
                    threadException = e;
                }
                finally {
                    fileThreadTimer.Stop();
                }
            });
        }


    }
}
