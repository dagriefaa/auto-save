using Modding;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AutoSave {
    public class Mod : ModEntryPoint {
        public const string MOD_NAME = "AutoSave";
        public static GameObject ModControllerObject;

        public static ModKey SaveKey;

        static AutoSaver autoSaver;

        public override void OnLoad() {
            // Init common mod controller object
            ModControllerObject = GameObject.Find("ModControllerObject");
            if (!ModControllerObject) { UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject")); }

            autoSaver = ModControllerObject.AddComponent<AutoSaver>();

            SaveKey = ModKeys.GetKey("Force Autosave");

            // Load Object Explorer mappings
            if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
                ModControllerObject.AddComponent<Mapper>();
            }

            // OnSceneChanged
            Events.OnActiveSceneChanged += OnSceneChanged;
            if (StatMaster.isMP) {
                OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }
        }

        static void OnSceneChanged(Scene a, Scene b) {
            if (Mod.SceneNotPlayable()) {
                return;
            }
            autoSaver.OnSceneChanged();
        }

        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }

        /// <see href="https://stackoverflow.com/a/34006336" />
        public static int CustomHash(params int[] vals) {
            int hash = 1009;
            foreach (int i in vals) {
                hash = (hash * 9176) + i;
            }
            return hash;
        }

        public static GameObject CreateObject(string name, GameObject parent, params Type[] components) {
            GameObject obj = new GameObject(name, components);
            obj.transform.SetParent(parent.transform, false);
            return obj;
        }

        public static Transform CreateObject(string name, Transform parent, params Type[] components) {
            GameObject obj = new GameObject(name, components);
            obj.transform.SetParent(parent, false);
            return obj.transform;
        }

        public static Transform FindObject(string path) {
            string[] parts = path.Split(new[] { "/" }, 2, StringSplitOptions.None);
            Transform root = Resources.FindObjectsOfTypeAll<Transform>()
                .Where(x => !x.parent && x.name == parts.First())
                .OrderBy(x => x.gameObject.scene == SceneManager.GetActiveScene() ? -1 : 1)
                .FirstOrDefault();
            return root == null || parts.Length < 2
                ? root
                : root.FindChild(parts.Last());
        }
    }
}
