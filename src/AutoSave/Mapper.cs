﻿using ObjectExplorer.Mappings;
using System;
using UnityEngine;

namespace AutoSave {
    internal class Mapper : MonoBehaviour {

        void Start() {
            ObjectExplorer.ObjectExplorer.AddMappings("AutoSave",
                new MInt<AutoSaver>("Autosave Interval (seconds)", c => c.SaveIntervalSeconds, (c, x) => c.SaveIntervalSeconds = x),
                new MInt<AutoSaver>("Delete Older Than (days)", c => c.DeleteIntervalDays, (c, x) => c.DeleteIntervalDays = x),
                new MInt<AutoSaver>("Max Files Per Machine", c => c.MaxFilesPerMachine, (c, x) => c.MaxFilesPerMachine = x),
                new MBool<AutoSaver>("Save Thumbnails", c => c.SaveThumbnails, (c, x) => c.SaveThumbnails = x),
                new MInt<AutoSaver>("Next Run (seconds)", c => ((int)(c.NextRun - DateTime.Now).TotalSeconds), isVisible: c => c.MachineUpdatedSinceLastSave)
            );
            Destroy(this);
        }
    }
}